package com.company;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int x = 1;

        do {
            try {
                calculate();

                x = 2;
            } catch (ArithmeticException e) {
                System.out.println("Error found in method calculate");
                System.out.println("Please Input 2 numbers that divide equally...");
            }
        }while (x == 1);

    }

    static void calculate() {
        Scanner input = new Scanner (System.in);
        System.out.print("Input the first number: ");
        int a = input.nextInt();

        System.out.print("Input the second number: ");
        int b = input.nextInt();

        int c = (a/b);
        int j = a%b;

        System.out.println();
        System.out.println("The division of input 1 and input 2 is: " + c);


        if(a%b != 0) {
            throw new ArithmeticException("You have to input two numbers that divide equally");
        }
        else {
            System.out.println("You have input two numbers that divide equally");
        }

    }
//        try {
//            FileInputStream f = new FileInputStream("hello.txt");
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } finally {
//
//        }
//
//        try {
//            Integer.parseInt("Hello");
//        } catch (NumberFormatException e) {
//            System.out.println("Unable to format");
//        }

//        boolean working = false;
//
//        if (!working) {
//            try {
//                throw new Exception();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//
//        System.out.print("What is your name?: ");
//        Scanner in = new Scanner (System.in);
//        String response;
//
//        do{
//            response = in.nextLine();
//        }
//        while (!response.equals("Caleb"));
//
//        System.out.println("hey there Caleb");
//    }
}
