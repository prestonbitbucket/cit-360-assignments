package com.company;

public class DVD {
    private String title;
    private String director;

    public DVD(String title, String director) {
        this.title = title;
        this.director = director;
    }

    public  String toString() {
        return "Title: " + title + " Director: " + director;
    }
}
