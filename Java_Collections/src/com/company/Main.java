package com.company;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        System.out.println("--List--");
	    List list = new ArrayList();
        list.add("Hi");
        list.add("my");
        list.add("name");
        list.add("is");
        list.add("Preston");
        list.add("Gunter");

        for (Object str : list) {
            System.out.println((String)str);
        }

        System.out.println("--Set--");
        Set set = new TreeSet();
        set.add("Hi");
        set.add("my");
        set.add("name");
        set.add("is");
        set.add("Preston");
        set.add("Gunter");

        for (Object str : set) {
            System.out.println((String)str);
        }

        System.out.println("--Queue--");
        Queue queue = new PriorityQueue();
        queue.add("Hi");
        queue.add("my");
        queue.add("name");
        queue.add("is");
        queue.add("Preston");
        queue.add("Gunter");

        Iterator iterator = queue.iterator();
        while (iterator.hasNext()) {
            System.out.println(queue.poll());
        }

        System.out.println("--Map--");
        Map map = new HashMap();
        map.put(1,"Preston");
        map.put(2,"Gunter");
        map.put(3,"Hi");
        map.put(4,"my");
        map.put(5,"name");
        map.put(4,"is");

        for (int i = 1; i < 6; i++) {
            String result = (String) map.get(i);
            System.out.println(result);
        }

        System.out.println("--List using Generics--");
        List<DVD> myList = new LinkedList<DVD>();
        myList.add(new DVD("BladeRunner 2049", "Some Dude"));
        myList.add(new DVD("There will be blood", "Some Dude"));
        myList.add(new DVD("The Dark Knight", "Christopher Nolan"));
        myList.add(new DVD("Shrek 2", "Andrew Adamson"));

        for (DVD dvd : myList) {
            System.out.println(dvd);
        }


    }

    }

