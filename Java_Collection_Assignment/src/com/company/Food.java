package com.company;

public class Food {
    private String foodName;
    private String restaurant;

    public Food(String foodName, String restaurant) {
        this.foodName = foodName;
        this.restaurant = restaurant;
    }
//This toString formats what you see in the console
    public  String toString() {
        return "Food Name: " + foodName + " restaurant: " + restaurant;
    }
}

