package com.company;
//Import your Java Classes
import java.util.*;
public class Main {

    public static void main(String[] args) {
        //Example of list
        //We make a list the add each new item in the list to an array
        // We then loop through the Array
        System.out.println("-- My List -- ");
        List list = new ArrayList();
        list.add("Hello");
        list.add("my");
        list.add("name");
        list.add("is");
        list.add("Preston");
        list.add("Gunter");
        list.add("I");
        list.add("am");
        list.add("from");
        list.add("Texas");
        list.add(" ");

        for(Object str : list) {
            System.out.println((String)str);
        }

        System.out.println("-- My Set --");
        //Example of set
        //Here we make a set
        //and we loop through the set using a for loop
        //Im not sure why you would want to use a TreeSet because it formats it a little differently
        Set set = new TreeSet();
        set.add("Hello");
        set.add("my");
        set.add("name");
        set.add("is");
        set.add("Preston");
        set.add("Gunter");
        set.add("I");
        set.add("am");
        set.add("from");
        set.add("Texas");

        for (Object str: set) {
            System.out.println((String)str);
        }

        //Example of a Queue
        //You can see that the order of the number matters.
        //If the number in the Hashmap repeats then the last number to be repeated is the only one that shows up
        //For example when 4 is used the word "is" was the only thing that showed up.
        //If we change line 57 to something other than number 4 then the word "my" will show up
        System.out.println("-- My Queue --");
        Map map = new HashMap();
        map.put(1,"Preston");
        map.put(2,"Gunter");
        map.put(3,"Hi");
        map.put(4,"my");
        map.put(5,"name");
        map.put(4,"is");
        map.put(6, " ");

        for(int i = 1; i < 7; i++) {
            String result = (String) map.get(i);
            System.out.println(result);
        }

        //In Generics we have to specify what kind of data we put in our collection
        //We specify what we put in out list by using brackets
        //Using Generics makes sure we put in the correct type in out list and keeps everything neat
        //Notice when we do our list this way it will be formatted with the FoodName and the restaurant the food is from
        System.out.println("--List using Generics--");
        List<Food> myList = new LinkedList<Food>();
        myList.add(new Food("Street Tacos", "Del Taco"));
        myList.add(new Food("French Toast", "I-HOP"));
        myList.add(new Food("Pancakes", "Waffle House"));
        myList.add(new Food("Fish Tacos", "Taco Bell"));

        for (Food foodName : myList) {
            System.out.println(foodName);
        }
    }


}
